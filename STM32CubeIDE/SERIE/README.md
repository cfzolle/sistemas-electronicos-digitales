Ejemplos de comunicaciones serie

abg_uart_polling: ejemplo sencillo de comunicaciones por el UART2 con un PC

abg_uart_int: ejemplo de uso de interrupciones, UART2 y ringbuffer

abg_spi_acc: ejemplo de comunicación SPI con el giroscopo l3gd20 integrado en la placa Discovery
